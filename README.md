# WorkAdventure Map Fachschaft

Das ist eine [WorkAdventure](https://workadventu.re) Map unserer Fachschaftsräume.

Die fertige/stabile Version findet sich immer in unserer [Fachschafts Welt](https://world.fachschaft.techfak.de).

Den Entwicklungsversion, so wie sie in diesem Ordner liegt, ist erreichbar über den Button "pipeline passed" oben auf dieser Seite, _falls_ er grün ist. (Dann hat das erstellen der Test-Maps auf dem Server geklappt.)

## Map bearbeiten

Lade dir den [Tiled Editor](https://www.mapeditor.org/) runter und öffne die Map damit.

Wir haben eine eigene [Anleitung zum Bearbeiten der Maps in unserem Wiki erstellt](https://fachschaft.techfak.de/howto/world-maps).

Wenn dort doch Fragen offen bleiben, gibt es auch die [Anleitung vom rc3](https://howto.rc3.world/maps.html#maps) und die [offizielle WorkAdventure Doku](https://workadventu.re/map-building/wa-maps).

Hier noch ein paar offizielle Ressourcen zum Editor:

- [Tiled documentation](https://doc.mapeditor.org/en/stable/manual/introduction/)
- [Tiled video tutorials](https://www.gamefromscratch.com/post/2015/10/14/Tiled-Map-Editor-Tutorial-Series.aspx)

### Tilesets

Tilesets sind die Grafikdateien, aus denen jede Map besteht. 

Einige Tilesets findet ihr im Ordner `tilesets`. In der `README.md` dort findet ihr jeweils den Link zur Originaldatei und die ursprüngliche Lizenz. Tragt neu hinzugefügte Tilesets dort bitte mit ein.

Auch zu Tile(set)s hat der [CCC ein paar Infos im Wiki](https://howto.rc3.world/maps.html#tiles), sowohl dazu wo man einfach neue findet, oder auch selbst welche erstellen kann.

## Map erstellen und ausprobieren

### Du weißt was `git` ist

1. Forke dieses Projekt

2. Clone das Projekt, kopier die Datei `template.json` und benenne sie so, wie deine Map heißen soll.  
(Oder optional: erstelle eine ganz eigene neue Map, ein paar Infos dazu [hier](https://howto.rc3.world/maps.html#maps))

3. Bearbeite die Map wie oben beschreiben und push die Änderungen.

4. Teste deine Map unter `https://play.world.fachschaft.techfak.de/_/global/DEINUSER.pages.ub.uni-bielefeld.de/wa-map-fachschaft/DEINEMAP.json`, wobei du DEINUSER durch deinen GitLab-Username ersetzt und DEINEMAP.json durch den Namen deiner Map.

5. Wenn du magst, füg auch in der Map `halle.json` ein Portal/Link zu deiner neuen Map hinzu.

### Mit so wenig technischem Kram wie möglich

1. Melde dich auf dieser Seite an. Das geht oben rechts mit deinem BITS-Login.

2. Klicke in diesem Projekt oben rechts auf "Fork". Dann wählst du den blauen Button "Auswählen" unter deinem Namen. So erstellst du eine persönliche Kopie (=Fork) dieser Maps, an der du ganz in Ruhe basteln kannst.  
![Fork Button](img/fork.png) ![Fork-Ziel auswählen](img/fork2.png)

3. Downloade alle Dateien über den Herunterladen-Button (Ein Pfeil nach unten mit U-förmiger Box darunter). Jetzt arbeitest du erst mal mit den herunter geladenen Dateien weiter.  
![Download Button](img/download.png)

4. Kopiere die Datei `template.json` und benenne sie so, wie deine Map heißen soll.  
(Oder optional: erstelle eine ganz eigene neue Map, ein paar Infos dazu [hier](https://howto.rc3.world/maps.html#maps))

5. Bearbeite die Map, wie oben beschreiben.

6. Geh jetzt wieder auf deinen Fork dieses Projektes. Du findest ihn oben links unter Projekte -> Deine Projekte -> Dein Name / WorkAdventure Map Fachschaft

7. Lade deine neue Map über das `+` -> "Datei hochladen" oben in der Mitte hoch. Hast du eine existierende Datei bearbeitet und möchtest sie ersetzen, kannst du das tun, indem du im Browser auf die Datei klickst und oben rechts den Button "ersetzen" bzw. "replace" benutzt.  
![Upload Button](img/actions.png)

8. Schau dir deine Map an unter `https://play.world.fachschaft.techfak.de/_/global/DEINUSER.pages.ub.uni-bielefeld.de/wa-map-fachschaft/DEINEMAP.json`, wobei du DEINUSER durch deinen GitLab-Username ersetzt und DEINEMAP.json durch den Namen deiner Map. Deinen GitLab-Username kannst du sehen, wenn du oben rechts auf dein Profilbild klickst, unter deinem Namen, ohne das @ davor.

9. Wenn du magst, füg auch in der Map `halle.json` ein Portal/Link zu deiner neuen Map hinzu.


## Map veröffentlichen

Um deine Map dann dauerhaft in der [Fachschafts Welt](https://world.fachschaft.techfak.de) verfügbar zu machen, schick sie entweder einfach per Mail oder per TeamChat an die Fachschaft Technik.

Alternativ kannst du auch gerne hier einen Merge-Request öffnen.
